// Models -> singular e maiusculo
// Models -> inserir no banco
const mongoose = require("mongoose")
const Schema = mongoose.Schema

const Categoria = new Schema({
    nombre: {
        type: String,
        require: true
    },
    slug: {
        type: String,
        require: true
    },
    date: {
        type: Date,
        default: Date.now
    }
})

mongoose.model("categorias", Categoria)