
const router = require('express').Router()
const mongoose = require('mongoose')
require('../models/categoria')
const Categoria = mongoose.model("categorias") // Passa os valores para a variavel
// Rotas

router.post('/', (req, res) => {
	const novaCategoria ={
		nombre: "roberto",
		slug: "rbt",
        date: "12/08/99"
	}
    new Categoria(novaCategoria).save().then((r) => {
        console.log(r)
	    res.send(200)
	}).catch((e) => {
		console.log(e)
        res.send(500)
	})  
})

module.exports = router